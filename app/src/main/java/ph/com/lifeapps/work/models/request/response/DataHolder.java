package ph.com.lifeapps.work.models.request.response;

/**
 * Created by Lem on 7/27/2016.
 */
public class DataHolder<K> {
    private boolean success;
    private K data;
    private String response_message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public K getData() {
        return data;
    }

    public void setData(K data) {
        this.data = data;
    }

    public String getResponse_message() {
        return response_message;
    }

    public void setResponse_message(String response_message) {
        this.response_message = response_message;
    }
}
