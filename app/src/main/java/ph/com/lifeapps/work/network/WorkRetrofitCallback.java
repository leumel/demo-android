package ph.com.lifeapps.work.network;

import ph.com.lifeapps.work.models.request.response.DataHolder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lem on 7/27/2016.
 */
public abstract class WorkRetrofitCallback<K> implements Callback<DataHolder<K>> {

    @Override
    public final void onResponse(Call<DataHolder<K>> call, Response<DataHolder<K>> response) {
        if (!response.body().isSuccess()){
            isFailed(response.body().getResponse_message());
        }else {
            isSuccess(response.body().getData());
        }
    }

    @Override
    public final void onFailure(Call<DataHolder<K>> call, Throwable t) {
        isFailed("Some server problem happened");
    }

    public abstract void isSuccess(K object);
    public abstract void isFailed(String errormessage);
}
