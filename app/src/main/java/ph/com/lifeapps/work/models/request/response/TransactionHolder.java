package ph.com.lifeapps.work.models.request.response;

/**
 * Created by Lem on 7/27/2016.
 */
public class TransactionHolder {
    private Transaction transaction;
    private int queue_length;

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public int getQueue_length() {
        return queue_length;
    }

    public void setQueue_length(int queue_length) {
        this.queue_length = queue_length;
    }
}
