package ph.com.lifeapps.work;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Lem on 7/27/2016.
 */
public class WorkApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("Aileron-Regular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
