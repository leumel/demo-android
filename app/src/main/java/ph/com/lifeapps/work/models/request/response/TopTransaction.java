package ph.com.lifeapps.work.models.request.response;

/**
 * Created by Lem on 8/18/2016.
 */
public class TopTransaction {
    private int id;
    private int counter_id;
    private String customer_name;
    private String contact_num;
    private int status;
    private int num_sms_sent;
    private long transaction_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCounter_id() {
        return counter_id;
    }

    public void setCounter_id(int counter_id) {
        this.counter_id = counter_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getContact_num() {
        return contact_num;
    }

    public void setContact_num(String contact_num) {
        this.contact_num = contact_num;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getNum_sms_sent() {
        return num_sms_sent;
    }

    public void setNum_sms_sent(int num_sms_sent) {
        this.num_sms_sent = num_sms_sent;
    }

    public long getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(long transaction_id) {
        this.transaction_id = transaction_id;
    }
}
