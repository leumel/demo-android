package ph.com.lifeapps.work.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.com.lifeapps.work.ContinuousRefresh;
import ph.com.lifeapps.work.R;
import ph.com.lifeapps.work.models.request.response.Counter;
import ph.com.lifeapps.work.models.request.response.DataHolder;
import ph.com.lifeapps.work.models.request.response.TopTransaction;
import ph.com.lifeapps.work.models.request.response.Transaction;
import ph.com.lifeapps.work.models.request.response.TransactionHolder;
import ph.com.lifeapps.work.network.WorkClient;
import ph.com.lifeapps.work.network.WorkRetrofitCallback;
import ph.com.lifeapps.work.utils.WorkUtils;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FullscreenActivity extends AppCompatActivity implements ContinuousRefresh.OnRefreshListener {

    @BindView(R.id.et_name)                     EditText etName;
    @BindView(R.id.et_contact_no)               EditText etContactNo;
    @BindView(R.id.ll_parentview)               LinearLayout llParentView;
    @BindView(R.id.tv_status)                   TextView tvStatus;
    @BindView(R.id.tv_serving)                  TextView tvServing;
    @BindView(R.id.tv_inline_count)             TextView tvInlineCount;

    private ContinuousRefresh continuousRefresh;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        ButterKnife.bind(this);
        continuousRefresh = ContinuousRefresh.getInstance(this);
    }

    @OnClick(R.id.btn_queue)
    public final void queue(){
        String contactNum = etContactNo.getText().toString();
        String userName = etName.getText().toString();

        if (userName.length() == 0){
            showSnackBar("Please input a name");
            return;
        }

        if(!WorkUtils.checkNumber(contactNum)){
            showSnackBar("Invalid phone number");
            return;
        }
        progressDialog = ProgressDialog.show(this, "Loading...", "Lining you up", false);
        WorkClient.getInstance().getService().createTranstaction(userName, contactNum.replaceFirst("0", "63"), "1").enqueue(new WorkRetrofitCallback<TransactionHolder>() {
            @Override
            public void isSuccess(TransactionHolder object) {
                progressDialog.dismiss();
                showSnackBar("Hi, "+object.getTransaction().getCustomer_name()+"! Wait for the message for confirmation");
                etName.setText("");
                etContactNo.setText("");
            }

            @Override
            public void isFailed(String errormessage) {
                progressDialog.dismiss();
                showSnackBar(errormessage);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void showSnackBar(String message){
        Snackbar.make(llParentView, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onRefreshSuccessful(Counter counter, int length) {
        if(counter.getTop_transaction()==null){
            tvStatus.setText("None");
            tvServing.setText("In Line");
            tvInlineCount.setText(Integer.toString(length));
            return;
        }

        TopTransaction topTransaction = counter.getTop_transaction();

        String status = topTransaction.getStatus() == 1 ? "Waiting" : "Ongoing";
        tvStatus.setText(status);
        tvServing.setText(topTransaction.getCustomer_name());
        tvInlineCount.setText(Integer.toString(length));
    }
}
