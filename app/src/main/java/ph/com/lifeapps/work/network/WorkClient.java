package ph.com.lifeapps.work.network;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lem on 7/25/2016.
 */
public class WorkClient {

    private static WorkClient workClient;
    private WorkService workService;


    private WorkClient(){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://ec2-54-169-242-132.ap-southeast-1.compute.amazonaws.com/")
                .build();

        workService = retrofit.create(WorkService.class);
    }

    public static WorkClient getInstance(){
        if (workClient==null){
            workClient = new WorkClient();
        }
        return workClient;
    }

    public WorkService getService(){
        return workService;
    }

}
