package ph.com.lifeapps.work.models.request.response;

import java.util.List;

/**
 * Created by Lem on 8/18/2016.
 */
public class CounterListHolder {
    private List<Counter> counters;

    public List<Counter> getCounters() {
        return counters;
    }

    public void setCounters(List<Counter> counters) {
        this.counters = counters;
    }
}
