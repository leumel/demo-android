package ph.com.lifeapps.work.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Lem on 7/27/2016.
 */
public class WorkUtils {

    public static boolean checkNumber(String number){
        Pattern pattern = Pattern.compile("(09[0-9]{9}){1}");
        Matcher matcher = pattern.matcher(number);
        return matcher.matches();
    }
}
