package ph.com.lifeapps.work.network;

import ph.com.lifeapps.work.models.request.response.CounterListHolder;
import ph.com.lifeapps.work.models.request.response.DataHolder;
import ph.com.lifeapps.work.models.request.response.TransactionHolder;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Lem on 7/25/2016.
 */
public interface WorkService {

    @FormUrlEncoded
    @POST("transaction/create")
    Call<DataHolder<TransactionHolder>> createTranstaction(@Field("name")String name, @Field("contact_number")String contact_number, @Field("counter_id")String counterID);

    @POST("transaction/check_top_queue")
    Call<DataHolder<CounterListHolder>> getOnTopQueue();
}
