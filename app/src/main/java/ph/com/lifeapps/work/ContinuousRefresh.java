package ph.com.lifeapps.work;

import android.os.Handler;

import java.util.concurrent.atomic.AtomicBoolean;

import ph.com.lifeapps.work.models.request.response.Counter;
import ph.com.lifeapps.work.models.request.response.CounterListHolder;
import ph.com.lifeapps.work.models.request.response.Transaction;
import ph.com.lifeapps.work.models.request.response.TransactionHolder;
import ph.com.lifeapps.work.network.WorkClient;
import ph.com.lifeapps.work.network.WorkRetrofitCallback;

/**
 * Created by Lem on 7/27/2016.
 */
public class ContinuousRefresh {

    private final int DELAY = 3000;

    public interface OnRefreshListener{
        void onRefreshSuccessful(Counter counter, int length);
    }

    private Runnable runnable;

    private Handler handler;
    private OnRefreshListener onRefreshListener;

    private AtomicBoolean shouldStop = new AtomicBoolean(false);
    private static ContinuousRefresh continuousRefresh;

    private ContinuousRefresh(OnRefreshListener onRefreshListener){
        this.onRefreshListener = onRefreshListener;
        handler = new Handler();
        refreshRunnable();
        startRefresh();
    }

    public static ContinuousRefresh getInstance(OnRefreshListener onRefreshListener){
        if (continuousRefresh == null){
            continuousRefresh = new ContinuousRefresh(onRefreshListener);
        }
        return continuousRefresh;
    }

    public void startRefresh(){
        handler.postDelayed(runnable, DELAY);
    }

    public void stopRunnable(){
        shouldStop.set(true);
        handler.removeCallbacks(runnable);
        refreshRunnable();
    }

    public void restartRunnable(OnRefreshListener onRefreshListener){
        this.onRefreshListener = onRefreshListener;
        stopRunnable();
        shouldStop.set(false);
        startRefresh();
    }

    private void refreshRunnable(){
        runnable = new Runnable() {
            @Override
            public void run() {
                if (!shouldStop.get()){
                    WorkClient.getInstance().getService().getOnTopQueue().enqueue(new WorkRetrofitCallback<CounterListHolder>() {
                        @Override
                        public void isSuccess(CounterListHolder object) {
                            if(object.getCounters()==null || object.getCounters().size() == 0){
                                return;
                            }

                            Counter counter = object.getCounters().get(0);
                            if (onRefreshListener!=null){
                                onRefreshListener.onRefreshSuccessful(counter, counter.getQueue_length());
                            }
                            startRefresh();
                        }

                        @Override
                        public void isFailed(String errormessage) {
                            startRefresh();
                        }
                    });
                }
            }
        };

    }

}
