package ph.com.lifeapps.work.models.request.response;

/**
 * Created by Lem on 7/27/2016.
 */
public class Transaction {
    private String customer_name;
    private String contact_num;
    private int status;
    private int id;
    private int num_sms_sent;

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getContact_num() {
        return contact_num;
    }

    public void setContact_num(String contact_num) {
        this.contact_num = contact_num;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum_sms_sent() {
        return num_sms_sent;
    }

    public void setNum_sms_sent(int num_sms_sent) {
        this.num_sms_sent = num_sms_sent;
    }
}
