package ph.com.lifeapps.work.models.request.response;

/**
 * Created by Lem on 8/18/2016.
 */
public class Counter {
    private int counter_id;
    private String counter_name;
    private TopTransaction top_transaction;
    private int queue_length;

    public int getCounter_id() {
        return counter_id;
    }

    public void setCounter_id(int counter_id) {
        this.counter_id = counter_id;
    }

    public String getCounter_name() {
        return counter_name;
    }

    public void setCounter_name(String counter_name) {
        this.counter_name = counter_name;
    }

    public int getQueue_length() {
        return queue_length;
    }

    public void setQueue_length(int queue_length) {
        this.queue_length = queue_length;
    }

    public TopTransaction getTop_transaction() {
        return top_transaction;
    }

    public void setTop_transaction(TopTransaction top_transaction) {
        this.top_transaction = top_transaction;
    }
}
